module gitlab.com/crdc/apex/master

require (
	github.com/coreos/go-etcd v2.0.0+incompatible // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-bindata/go-bindata v1.0.0 // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/google/uuid v1.0.0
	github.com/graph-gophers/dataloader v5.0.0+incompatible
	github.com/graph-gophers/graphql-go v0.0.0-20181128220952-0079757a4d96
	github.com/hashicorp/golang-lru v0.5.1
	github.com/jmoiron/sqlx v0.0.0-20181024163419-82935fac6c1a
	github.com/lib/pq v1.0.0
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/opentracing/opentracing-go v1.0.2 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/rs/xid v1.2.1
	github.com/spf13/viper v1.4.0
	gitlab.com/crdc/apex/apexctl v0.0.0-20190618072405-44b84bdea789
	gitlab.com/crdc/apex/go-apex v0.1.20
	golang.org/x/crypto v0.0.0
	golang.org/x/net v0.0.0
	google.golang.org/grpc v1.21.0
)

replace golang.org/x/lint v0.0.0 => github.com/golang/lint v0.0.0-20190301231843-5614ed5bae6f

replace golang.org/x/crypto v0.0.0 => github.com/golang/crypto v0.0.0-20190308221718-c2843e01d9a2

replace golang.org/x/tools v0.0.0 => github.com/golang/tools v0.0.0-20190311215038-5c2858a9cfe5

replace golang.org/x/oauth2 v0.0.0 => github.com/golang/oauth2 v0.0.0-20190226205417-e64efc72b421

replace golang.org/x/net v0.0.0 => github.com/golang/net v0.0.0-20190311183353-d8887717615a

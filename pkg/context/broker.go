package context

import (
	"fmt"
	"log"

	pb "gitlab.com/crdc/apex/go-apex/proto/v1"

	"google.golang.org/grpc"
	//"google.golang.org/grpc/credentials"
)

func BrokerClient(host string, port int) (pb.EndpointClient, error) {
	log.Printf("Connecting to broker service at %s:%d\n", host, port)

	addr := fmt.Sprintf("%s:%d", host, port)

	// Create an insecure connection
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("could not dial: %s: %s", addr, err)
	}

	// Initialie the client connection
	return pb.NewEndpointClient(conn), nil
}

package context

import (
	"fmt"
	"log"

	pb "gitlab.com/crdc/apex/apexctl/proto"

	"google.golang.org/grpc"
)

func ManagerClient(config *Config) (pb.ManagerClient, error) {
	addr := fmt.Sprintf("%s:%d", config.Manager.Host, config.Manager.Port)
	log.Print("Connecting to Manager server at ", addr)

	// Create an insecure connection
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("could not dial: %s: %s", addr, err)
	}

	// Initialie the client connection
	return pb.NewManagerClient(conn), nil
}

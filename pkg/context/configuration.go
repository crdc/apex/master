package context

import (
	"fmt"
	"log"
	"strings"
	"time"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

type database struct {
	Host     string `mapstructure:"host"`
	Port     string `mapstructure:"port"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
	Name     string `mapstructure:"name"`
}

type auth struct {
	JWTSecret   string        `mapstructure:"jwt-secret"`
	JWTExpireIn time.Duration `mapstructure:"jwt-expire-in"`
}

type logging struct {
	Debug  bool   `mapstructure:"debug"`
	Format string `mapstructure:"format"`
}

type configure struct {
	Host   string `mapstructure:"host"`
	Port   string `mapstructure:"port"`
	UseTLS bool   `mapstructure:"tls"`
	CACert string `mapstructure:"cacert"`
	Cert   string `mapstructure:"cert"`
	Key    string `mapstructure:"key"`
}

type client struct {
	Host string `mapstructure:"host"`
	Port int    `mapstructure:"port"`
}

type Config struct {
	App       string            `mapstructure:"app"`
	DB        database          `mapstructure:"db"`
	Auth      auth              `mapstructure:"auth"`
	Log       logging           `mapstructure:"log"`
	Configure configure         `mapstructure:"configure"`
	Manager   client            `mapstructure:"manager"`
	Broker    map[string]client `mapstructure:"broker"`
}

func LoadConfig(path string) (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()
	config.SetConfigName("master")
	config.AddConfigPath(".")
	config.AddConfigPath("/etc/plantd")
	config.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANTD_MASTER")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c Config

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	log.Print("Configuration Location: ", config.ConfigFileUsed())

	return &c, nil
}

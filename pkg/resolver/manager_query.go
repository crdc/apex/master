package resolver

import (
	"errors"
	"fmt"

	gcontext "gitlab.com/crdc/apex/master/pkg/context"
	"gitlab.com/crdc/apex/master/pkg/service"
	"golang.org/x/net/context"
)

func (r *Resolver) ManagerStatus(ctx context.Context) (*[]*propertyResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	// Use the manager service
	apexResponse, err := ctx.Value("managerService").(*service.ManagerService).Status()
	if err != nil {
		fmt.Errorf("Manager error: %s", err)
		return nil, err
	}

	// Create an array of property resolvers
	var _properties []*propertyResolver
	for _, property := range *apexResponse {
		_properties = append(_properties, &propertyResolver{property})
	}

	return &_properties, nil
}

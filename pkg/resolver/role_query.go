package resolver

import (
	"errors"

	gcontext "gitlab.com/crdc/apex/master/pkg/context"
	"gitlab.com/crdc/apex/master/pkg/loader"
	"gitlab.com/crdc/apex/master/pkg/service"

	"github.com/op/go-logging"
	"golang.org/x/net/context"
)

func (r *Resolver) Role(ctx context.Context, args struct {
	Name string
}) (*roleResolver, error) {
	//Without using dataloader:
	//role, err := ctx.Value("roleService").(*service.RoleService).FindByName(args.Name)
	userId := ctx.Value("user_id").(*string)
	role, err := loader.LoadRole(ctx, args.Name)

	if err != nil {
		ctx.Value("log").(*logging.Logger).Errorf("Graphql error : %v", err)
		return nil, err
	}

	ctx.Value("log").(*logging.Logger).Debugf("Retrieved role by user_id[%s] : %v", *userId, *role)

	return &roleResolver{role}, nil
}

func (r *Resolver) Roles(ctx context.Context, args struct {
	First *int32
	After *string
}) (*rolesConnectionResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	userId := ctx.Value("user_id").(*string)

	roles, err := ctx.Value("roleService").(*service.RoleService).List(args.First, args.After)
	count, err := ctx.Value("roleService").(*service.RoleService).Count()
	ctx.Value("log").(*logging.Logger).Debugf("Retrieved roles by user_id[%s] :", *userId)
	config := ctx.Value("config").(*gcontext.Config)

	if config.Log.Debug {
		for _, role := range roles {
			ctx.Value("log").(*logging.Logger).Debugf("%v", *role)
		}
	}

	ctx.Value("log").(*logging.Logger).Debugf("Retrieved total roles count by user_id[%s] : %v", *userId, count)

	if err != nil {
		ctx.Value("log").(*logging.Logger).Errorf("Graphql error : %v", err)
		return nil, err
	}

	return &rolesConnectionResolver{
		roles:      roles,
		totalCount: count,
		from:       &(roles[0].ID),
		to:         &(roles[len(roles)-1].ID),
	}, nil
}

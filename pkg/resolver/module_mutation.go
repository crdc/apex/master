package resolver

import (
	"errors"
	"fmt"
	"strings"

	//"encoding/json"
	//"log"

	//"gitlab.com/crdc/apex/master/pkg/model"
	gcontext "gitlab.com/crdc/apex/master/pkg/context"
	"gitlab.com/crdc/apex/master/pkg/service"

	//"gitlab.com/crdc/apex/go-apex/api"
	pb "gitlab.com/crdc/apex/go-apex/proto/v1"

	"github.com/op/go-logging"
	"golang.org/x/net/context"
)

func (r *Resolver) SetModuleProperty(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
	Property    propertyInput
}) (*propertyResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	// Convert a property input into the pb Property
	convertedProperty := &pb.Property{
		Key:   args.Property.Key,
		Value: args.Property.Value,
	}

	property, err := ctx.Value("brokerService").(*service.BrokerService).SetModuleProperty(args.ServiceName, args.ModuleName, convertedProperty)
	if err != nil {
		fmt.Errorf("SetModuleProperty error: %s", err)
		return nil, err
	}

	return &propertyResolver{property}, nil
}

func (r *Resolver) SetModuleProperties(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
	Properties  *[]*propertyInput
}) (*[]*propertyResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	// Parse the properties into a pb property array
	var reqProperties []*pb.Property
	if args.Properties != nil {
		reqProperties = ConvertProperties(*args.Properties)
	}

	// Use the broker service
	properties, err := ctx.Value("brokerService").(*service.BrokerService).SetModuleProperties(args.ServiceName, args.ModuleName, reqProperties)
	if err != nil {
		fmt.Errorf("SetModuleProperties error: %s", err)
		return nil, err
	}

	// Create property resolvers for each property
	var _properties []*propertyResolver
	for _, property := range properties {
		_properties = append(_properties, &propertyResolver{property})
	}

	return &_properties, nil
}

func (r *Resolver) SetModuleRecipe(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
	RecipeID    string
}) (*[]*propertyResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	// Handle the situation where it is reset
	if args.RecipeID == "" {
		ctx.Value("log").(*logging.Logger).Debugf("Resetting module recipe")
		var resetProperties []*pb.Property
		resetProperties = append(resetProperties, &pb.Property{
			Key:   "recipe-id",
			Value: "",
		})
		resetProperties = append(resetProperties, &pb.Property{
			Key:   "recipe-name",
			Value: "",
		})
		resetProperties = append(resetProperties, &pb.Property{
			Key:   "recipe-raw",
			Value: "0,0,0,0,0,10",
		})

		// Use the broker service
		properties, err := ctx.Value("brokerService").(*service.BrokerService).SetModuleProperties(args.ServiceName, args.ModuleName, resetProperties)
		if err != nil {
			fmt.Errorf("SetModuleProperties error: %s", err)
			return nil, err
		}
		// Create property resolvers for each property
		var _properties []*propertyResolver
		for _, property := range properties {
			_properties = append(_properties, &propertyResolver{property})
		}

		return &_properties, nil
	}

	// Get the recipe
	conf, err := ctx.Value("confService").(*service.ConfigureService).FindById(args.RecipeID, "RECIPE")
	if err != nil {
		ctx.Value("log").(*logging.Logger).Debugf("GraphQL error : %v", err)
		return nil, err
	}

	// Get the properties from the recipe and create the request
	recipe := &configurationResolver{conf}
	var reqProperties []*pb.Property
	reqProperties = append(reqProperties, &pb.Property{
		Key:   "recipe-id",
		Value: args.RecipeID,
	})
	reqProperties = append(reqProperties, &pb.Property{
		Key:   "recipe-name",
		Value: *recipe.Name(),
	})
	// Convert the recipe from object to a string
	var rawRecipe []string
	for _, someObject := range *recipe.Objects() {
		var phaseString []string
		for _, someProperty := range *someObject.Properties() {
			phaseString = append(phaseString, *someProperty.Value())
		}
		rawRecipe = append(rawRecipe, strings.Join(phaseString, ","))
	}
	reqProperties = append(reqProperties, &pb.Property{
		Key:   "recipe-raw",
		Value: strings.Join(rawRecipe, "|"),
	})

	// Use the broker service
	properties, err := ctx.Value("brokerService").(*service.BrokerService).SetModuleProperties(args.ServiceName, args.ModuleName, reqProperties)
	if err != nil {
		fmt.Errorf("SetModuleProperties error: %s", err)
		return nil, err
	}

	// Create property resolvers for each property
	var _properties []*propertyResolver
	for _, property := range properties {
		_properties = append(_properties, &propertyResolver{property})
	}

	return &_properties, nil
}

package resolver

import (
	"gitlab.com/crdc/apex/master/pkg/model"
	"gitlab.com/crdc/apex/master/pkg/service"

	"github.com/op/go-logging"
	"golang.org/x/net/context"
)

func (r *Resolver) CreateRole(ctx context.Context, args *struct {
	Name string
}) (*roleResolver, error) {
	role := &model.Role{
		Name: args.Name,
	}

	role, err := ctx.Value("roleService").(*service.RoleService).CreateRole(role)
	if err != nil {
		ctx.Value("log").(*logging.Logger).Errorf("Graphql error : %v", err)
		return nil, err
	}
	ctx.Value("log").(*logging.Logger).Debugf("Created role : %v", *role)
	return &roleResolver{role}, nil
}

# Base image: https://hub.docker.com/_/golang/
FROM golang:1.11.5-alpine3.9
MAINTAINER Geoff Johnson <geoff.jay@gmail.com>

COPY etc/plantd/master.yaml /etc/plantd/
COPY plantd-master-static /usr/bin/plantd-master

CMD [ "/usr/bin/plantd-master" ]

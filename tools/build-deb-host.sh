#!/bin/bash
# This script needs to be called from the repository root.

VERSION="$(git describe | sed -e 's/[vV]//g')" # version number cannot start with char.
echo $VERSION
DESCRIPTION="Service that functions as the system controller and master for Apex"
LICENSE=$"MIT License"
URL=$"https://gitlab.com/crdc/apex/master"
MAINTAINER=$"Mirko Moeller <mirko.moeller@coanda.ca>"
VENDOR=$""

if which go; then
  : # colon means do nothing
else
  echo "golang not installed or not reachable"
  exit 1
fi

if which fpm; then
  fpm --input-type dir --output-type deb \
    --name plantd-master --version $VERSION \
    --description "$DESCRIPTION" \
    --license "$LICENSE" \
    --url "$URL" \
    --maintainer "$MAINTAINER" \
    --vendor "$VENDOR" \
    --deb-systemd init/plantd-master.service \
    --config-files etc/plantd/master.yaml bin=/usr/
else
  echo "fpm not installed or not reachable"
  exit 1
fi

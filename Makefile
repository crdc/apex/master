PROJECT=master
REGISTRY=registry.gitlab.com/crdc/apex/$(PROJECT)
DESTDIR=/usr
CONFDIR=/etc
SYSTEMDDIR=/lib/systemd

M := $(shell printf "\033[34;1m▶\033[0m")
TAG := $(shell git describe --all | sed -e's/.*\///g')
VERSION := $(shell git describe)
BINARY_NAME = "plantd-master"

all: build

print-version:
	@ echo $(VERSION)

lint: ; $(info $M Linting the files)
	@./tools/checks lint

test: ; $(info $M Running unittests)
	@./tools/checks test

race: ; $(info $M Running data race detector)
	@./tools/checks race

msan: ; $(info $M Running memory sanitizer)
	@./tools/checks msan

coverage: ; $(info $M Generating global code coverage report)
	@./tools/coverage

coverhtml: ; $(info $M Generating global code coverage report in HTML)
	@./tools/coverage html

build: schema ; $(info $(M) Building project...)
	@go build -o bin/$(BINARY_NAME) -ldflags "-X main.Version=$(VERSION)" ./cmd/master

build-deb: build; $(info $(M) Building DEB package)
	@./tools/build-deb-host.sh

setup: ; $(info $(M) Fetching github.com/golang/dep...)
	@go get -u github.com/go-bindata/go-bindata

schema: ; $(info $(M) Embedding schema files into binary...)
	@go generate ./api/schema

image: ; $(info $(M) Building application image...)
	@docker build -t $(REGISTRY) -f ./build/Dockerfile .

container: image ; $(info $(M) Running application container...)
	@docker run -p 5000:5000 $(REGISTRY):latest

# FIXME: this assumes master branch and os/arch, consider using build output
install: ; $(info $(M) Installing plantd $(PROJECT) service...)
	@install -Dm 755 bin/$(PROJECT)-$(TAG)-linux-amd64 "$(DESTDIR)/bin/plantd-$(PROJECT)"
	@install -Dm 644 README.md "$(DESTDIR)/share/doc/plantd/$(PROJECT)/README"
	@install -Dm 644 LICENSE "$(DESTDIR)/share/licenses/plantd/$(PROJECT)/COPYING"
	@mkdir -p "$(CONFDIR)/plantd"
	@mkdir -p /run/plantd
	@install -Dm 644 configs/$(PROJECT).yaml "$(CONFDIR)/plantd/$(PROJECT).yaml"
	@install -Dm 644 init/plantd-$(PROJECT).service "$(SYSTEMDDIR)/system/plantd-$(PROJECT).service"

uninstall: ; $(info $(M) Uninstalling plantd $(PROJECT) service...)
	@rm $(DESTDIR)/bin/plantd-$(PROJECT)

clean: ; $(info $(M) Removing generated files... )
	@rm api/schema/bindata.go
	@rm -rf bin/

.PHONY: all build build-deb print-version container coverage coverhtml setup schema image lint test race msan install uninstall clean
